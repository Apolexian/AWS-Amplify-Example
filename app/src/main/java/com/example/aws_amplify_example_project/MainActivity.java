package com.example.aws_amplify_example_project;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.amazonaws.amplify.generated.graphql.CreateTodoMutation;
import com.amazonaws.amplify.generated.graphql.ListTodosQuery;
import com.amazonaws.amplify.generated.graphql.OnCreateTodoSubscription;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import java.util.Objects;

import javax.annotation.Nonnull;

import type.CreateTodoInput;

public class MainActivity extends AppCompatActivity {
    private AWSAppSyncClient mAWSAppSyncClient;
    private AppSyncSubscriptionCall subscriptionWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Initialize the AppSync client
        mAWSAppSyncClient =
                AWSAppSyncClient.builder()
                        .context(getApplicationContext())
                        .awsConfiguration(new AWSConfiguration(getApplicationContext()))
                        .build();
    }

    public void runMutation() {
        CreateTodoInput createTodoInput =
                CreateTodoInput.builder()
                        .name("Use AppSync")
                        .description("Realtime and Offline")
                        .build();

        mAWSAppSyncClient
                .mutate(CreateTodoMutation.builder().input(createTodoInput).build())
                .enqueue(mutationCallback);
    }

    private GraphQLCall.Callback<CreateTodoMutation.Data> mutationCallback =
            new GraphQLCall.Callback<CreateTodoMutation.Data>() {
                @Override
                public void onResponse(@Nonnull Response<CreateTodoMutation.Data> response) {
                    Log.i("Results", "Added Todo");
                }

                @Override
                public void onFailure(@Nonnull ApolloException e) {
                    Log.e("Error", e.toString());
                }
            };

    public void runQuery() {
        mAWSAppSyncClient
                .query(ListTodosQuery.builder().build())
                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                .enqueue(todosCallback);
    }

    private GraphQLCall.Callback<ListTodosQuery.Data> todosCallback =
            new GraphQLCall.Callback<ListTodosQuery.Data>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(@Nonnull Response<ListTodosQuery.Data> response) {
                    assert response.data() != null;
                    Log.i(
                            "Results",
                            Objects.requireNonNull(
                                            Objects.requireNonNull(response.data().listTodos())
                                                    .items())
                                    .toString());
                }

                @Override
                public void onFailure(@Nonnull ApolloException e) {
                    Log.e("ERROR", e.toString());
                }
            };

    private void subscribe() {
        OnCreateTodoSubscription subscription = OnCreateTodoSubscription.builder().build();
        subscriptionWatcher = mAWSAppSyncClient.subscribe(subscription);
        subscriptionWatcher.execute(subCallback);
    }

    private AppSyncSubscriptionCall.Callback subCallback =
            new AppSyncSubscriptionCall.Callback() {
                @Override
                public void onResponse(@Nonnull Response response) {
                    assert response.data() != null;
                    Log.i("Response", response.data().toString());
                }

                @Override
                public void onFailure(@Nonnull ApolloException e) {
                    Log.e("Error", e.toString());
                }

                @Override
                public void onCompleted() {
                    Log.i("Completed", "Subscription completed");
                }
            };
}
